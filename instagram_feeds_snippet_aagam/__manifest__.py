# -*- encoding: utf-8 -*-
{
    'name': 'Instagram snippet in odoo, instagram feed post with comments and likes in odoo website',
    'category': 'Website',
    'version': '14.0.1.1.0',
    'summary': 'Responsive instgram feed snippet with comments and likes + '
    'supported in Odoo v10, v11, v12, v13, v14',
    'description': """
        Instagram in odoo, instagram snippet for odoo website is allows to display instagram feed in odoo, like, comments and click to navigate in instagram page +
        supported in Odoo v11, v12, v13, v14
    """,
    'depends': ['base', 'web', 'web_editor', 'website', ],
    'data': [
        'views/res_config_settings_views.xml',
        'views/snippets.xml',
    ],
    'demo': [],
    'price': 39.48,
    'currency': 'EUR',
    'support': ': business@aagaminfotech.com',
    'author': 'Aagam Infotech',
    'website': 'http://aagaminfotech.com',
    'installable': True,
    'license': 'AGPL-3',
    'images': ['static/description/images/icon.png'],
}
