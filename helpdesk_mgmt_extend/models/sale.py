from odoo import _, api, fields, models


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    helpdesk_ticket_id = fields.Many2one('helpdesk.ticket', 'Helpdesk Ticket',
                                            copy=False)
