from odoo import api, fields, models


class HelpdeskTicket(models.Model):

    _inherit = "helpdesk.ticket"

    sale_id = fields.Many2one(string="Sale Order", comodel_name="sale.order")
    sale_line_id = fields.Many2one(string="Sale Order Line",
                                    comodel_name="sale.order.line")


    @api.onchange('sale_id')
    def empty_sale_line(self):
        self.sale_line_id = False


    @api.model
    def create(self, vals):
        if vals.get("sale_line_id"):
            try:
                vals["sale_id"] = self.env['sale.order.line'].browse(vals["sale_line_id"]).order_id.id
            except Exception as e:
                pass
        return super().create(vals)


    def call_create_so(self):
        return {
            'name': 'New Sale Order for Helpdesk Ticket',
            'type': 'ir.actions.act_window',
            'res_model': 'sale.order',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            'context': {
                        'create_so_helpdesk': self.id,
                        'default_partner_id': self.partner_id and self.partner_id.id or False,
                        'default_helpdesk_ticket_id': self.id,
                        },
        }
