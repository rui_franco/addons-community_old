from odoo import _, api, fields, models

class ProjectTemplate(models.Model):
    _inherit = "product.template"

    helpdesk = fields.Boolean('Helpdesk product?')
