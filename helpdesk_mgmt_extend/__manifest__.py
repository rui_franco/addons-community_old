{
    "name": "Helpdesk Project (extend)",
    "summary": "Extra features to CE helpdesk modules",
    "version": "13.0.1.0.0",
    "license": "AGPL-3",
    "category": "After-Sales",
    "author": "Wibtec",
    "website": "https://www.wibtec.com",
    "description": """
- creates a connection between sale order and sale order lines and helpdesk tickets
- creates a helpdesk ticket for each product in the SO that is set as "helpdesk"
""",
    "depends": [
                "helpdesk_mgmt_project",
                "helpdesk_mgmt_timesheet",
                "sale_management",
                "base_automation"
                ],
    "data": [
        "data/base_automation_data.xml",
        "views/helpdesk_ticket_view.xml",
        #"views/project_view.xml",
        #"views/project_task_view.xml",
        "views/product_view.xml",
        "views/sale_view.xml",
    ],
    "auto_install": False,
}
